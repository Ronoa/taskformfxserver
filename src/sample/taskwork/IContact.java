package sample.taskwork;

public interface IContact {
      void setName(String name);
      void setNumber(String number);
      String getName();
      String getNumber();

      String getContactToString();
      String toString();
}
