package sample.taskwork;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

public interface ITask {
    String getName();
    void setName(String name);
    String getDescription();
    void setDescription(String description);
    Calendar getDate();
    void setDate(Calendar date);
    ICList getContactList();
    String getDateString();
    void setContactList(ICList contactList);
    String getAllContactListString();

    boolean isYvedomlenie();
    void setYvedomlenie(boolean flag);
    int getOtlozhit();
    void setOtlozhit(int otlozhit);

    String toString();
}
