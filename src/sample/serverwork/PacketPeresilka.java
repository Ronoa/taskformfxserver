package sample.serverwork;

import sample.taskwork.*;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

public class PacketPeresilka extends OPacket {
    private ITask task;
    public PacketPeresilka(){}

    public PacketPeresilka(ITask task) {
        this.task = task;
    }
    @Override
    public short getId() {
        return 8;
    }

    @Override
    public void write(DataOutputStream dos) throws IOException {
        dos.writeUTF(task.getName());
        dos.writeUTF(task.getDescription());
        dos.writeLong(task.getDate().getTime().getTime());

        dos.writeBoolean(task.isYvedomlenie());
        dos.writeInt(task.getOtlozhit());

        int countIs= task.getContactList().getSize();
        dos.writeInt(countIs);

        for(int i=0;i<countIs;i++){
            dos.writeUTF(task.getContactList().getContactByIndex(i).getName());
            dos.writeUTF(task.getContactList().getContactByIndex(i).getNumber());
        }
System.out.println("PacketPeresilka (write) name:"+ task.getName() +" "
            +"flag:" +task.getOtlozhit() +" "
        +"st:" +task.isYvedomlenie());

    }

    @Override
    public void read(DataInputStream dis) throws IOException {
        String name=dis.readUTF();

        String des = dis.readUTF();

        Long lg=dis.readLong();

        int countIs= dis.readInt();

        ICList clist=new ContactList();
        for(int i=0;i<countIs;i++){
            String name1=dis.readUTF();
            String number1 = dis.readUTF();
            clist.addContactEnd(new Contact(name1,number1));
        }
        Date data = new Date(lg);
        Calendar cal =  Calendar.getInstance();
        cal.setTime(data);

        boolean flag=dis.readBoolean();
        int t=dis.readInt();
        ITask task2=new Task(name,des,cal,clist,flag,t);

        int iu= ServerLoader.getHandler(getSocket()).searchIndexTask(task2);
        System.out.println("PacketPeresilka (read) name:"+ name +" "
                +"flag:" +t +" "
                +"st:" +flag);

        ServerLoader.getHandler(getSocket()).tasklist.getTaskToIndex(iu).setOtlozhit(t);

        ServerLoader.getHandler(getSocket()).tasklist.getTaskToIndex(iu).setYvedomlenie(flag);

        ServerLoader.getHandler(getSocket()).serialaizeble();
        ServerLoader.sendPacket(getSocket(), new PacketTaskWork(ServerLoader.getHandler(getSocket()).tasklist, ServerLoader.getHandler(getSocket()).isp));

    }

    @Override
    public void handle() {

    }
}
