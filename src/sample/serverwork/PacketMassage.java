package sample.serverwork;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PacketMassage extends OPacket {
    private String sender;
    private String message;

    PacketMassage(){}

    PacketMassage(String sender, String message){
        this.sender=sender;
        this.message=message;

    }

    @Override
    public short getId() {
        return 2;
    }

    @Override
    public void write(DataOutputStream dos) throws IOException {
        dos.writeUTF(sender);
        dos.writeUTF(message);
    }

    @Override
    public void read(DataInputStream dis) throws IOException {

            message = dis.readUTF();

    }

    @Override
    public void handle() {
        sender=ServerLoader.getHandler(getSocket()).getNickname();
        ServerLoader.sendPacket(getSocket(),this);
     //   ServerLoader.handlers.keySet().forEach(s -> ServerLoader.sendPacket(s,this ));
        System.out.println(String.format("[%s] %s",sender,message));
//        Thread tr=new Thread(){
//          public void run(){
//              while (true){
//
//                  String s= new SimpleDateFormat("mm").format(Calendar.getInstance().getTime());
//                  System.out.println("mm: "+s);
//                    if(Integer.parseInt(s) > 35 )
//                        ServerLoader.sendPacket(getSocket(),new PacketMassage("server","tyt-35-yty"));
//                  try {
//                      Thread.sleep(1000);
//                  } catch (InterruptedException e) {
//                      e.printStackTrace();
//                  }
//              }
//
//          }
//        };
//        tr.start();
       //String s= new SimpleDateFormat("mm").format(Calendar.getInstance().getTime());
    }
}
