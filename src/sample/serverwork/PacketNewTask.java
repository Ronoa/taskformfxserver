package sample.serverwork;

import sample.taskwork.*;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

public class PacketNewTask extends OPacket {
    private ITask task;

    public PacketNewTask(ITask task) {
        this.task = task;
    }

    public PacketNewTask() {
    }

    @Override
    public short getId() {
        return 5;
    }

    @Override
    public void write(DataOutputStream dos) throws IOException {

    }

    @Override
    public void read(DataInputStream dis) throws IOException {
        String name=dis.readUTF();

        String des = dis.readUTF();

        Long lg=dis.readLong();

        int countIs= dis.readInt();

        ICList clist=new ContactList();
        for(int i=0;i<countIs;i++){
            String name1=dis.readUTF();
            String number1 = dis.readUTF();
            clist.addContactEnd(new Contact(name1,number1));
        }
        Date data = new Date(lg);
        Calendar cal =  Calendar.getInstance();
        cal.setTime(data);

        boolean flag=dis.readBoolean();
        int t=dis.readInt();


        ITask task2=new Task(name,des,cal,clist,flag,t);
        ServerLoader.getHandler(getSocket()).tasklist.addTaskEnd(task2);
        ServerLoader.getHandler(getSocket()).serialaizeble();
    }

    @Override
    public void handle() {

    }
}
