package sample.serverwork;

import sample.taskwork.ITask;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PacketYvedomlebie  extends OPacket{
    private ITask task;

    public PacketYvedomlebie() {
    }

    public PacketYvedomlebie(ITask task) {
        this.task = task;
    }

    @Override
    public short getId() {
        return 7;
    }

    @Override
    public void write(DataOutputStream dos) throws IOException {
        dos.writeUTF(task.getName());
        dos.writeUTF(task.getDescription());
        dos.writeLong(task.getDate().getTime().getTime());

        int countIs= task.getContactList().getSize();
        dos.writeInt(countIs);

        for(int i=0;i<countIs;i++){
            dos.writeUTF(task.getContactList().getContactByIndex(i).getName());
            dos.writeUTF(task.getContactList().getContactByIndex(i).getNumber());
        }

    }

    @Override
    public void read(DataInputStream dis) throws IOException {

    }

    @Override
    public void handle() {

    }
}
