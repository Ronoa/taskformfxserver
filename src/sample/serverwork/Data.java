package sample.serverwork;

import java.io.Serializable;
import java.util.HashMap;


public class Data implements Serializable {


    private  static HashMap<String,String> loginPassword= new HashMap<String, String>();

    public static HashMap<String, String> getLoginPasswordHP() {
        return loginPassword;
    }

    public static void setLoginPassword(HashMap<String, String> loginPassword) {
        Data.loginPassword = loginPassword;
    }

    public static void addLoginPassword(String login,String pass){
        Data.loginPassword.put(login,pass);
    }

    public static String getValueUseKey(String login){
        return Data.loginPassword.get(login);
    }

    public static boolean isPassRight(String nic, String pas){
        if(Data.loginPassword.containsKey(nic))
        {
        String temppas=getValueUseKey(nic);
        System.out.println(String.format(" isPassRight nic[%s] pas[%s] temppas[%s]  ",nic,pas,temppas));

        if(temppas.equals(pas)){
            return true;
        }
        else {
            return false;
        }
        }
        return  false;

    }
}
