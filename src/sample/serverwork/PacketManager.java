package sample.serverwork;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class PacketManager {

    private  final  static Map<Short,Class<? extends  OPacket>> packets = new HashMap();

    static {

        packets.put((short)1, PacketAuthorize.class);
        packets.put((short)2, PacketMassage.class);
        packets.put((short)3, PacketTaskWork.class);
        packets.put((short)4, PacketIspolnitelei.class);
        packets.put((short)5, PacketNewTask.class);
        packets.put((short)6, PacketRemoveTask.class);
        packets.put((short)7, PacketYvedomlebie.class);
        packets.put((short)8, PacketPeresilka.class);

    }

    public static OPacket getPacket(short id){
        try {
            return packets.get(id).newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        }
    }

    public  static void read(short id, DataInputStream dis){
       try{
           OPacket packet = packets.get(id).newInstance();
           packet.read(dis);
       } catch (IllegalAccessException e) {
           e.printStackTrace();
       } catch (IOException e) {
           e.printStackTrace();
       } catch (InstantiationException e) {
           e.printStackTrace();
       }

    }


}
