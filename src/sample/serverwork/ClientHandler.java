package sample.serverwork;

import sample.taskwork.ICList;
import sample.taskwork.ITList;
import sample.taskwork.ITask;
import sample.taskwork.TaskWork;

import java.io.*;
import java.net.Socket;

public class ClientHandler extends Thread {
    private final Socket client;
    private String nickname = "anonimys";
    private boolean correctPass = false;
    public ITList tasklist;
    public ICList isp;


    public ClientHandler(Socket client) {
        this.client = client;


    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    private void checkCorrect(String login, String pass) {
        correctPass = Data.isPassRight(login, pass);
    }

    public boolean isCorrect(String login, String pass) {
        checkCorrect(login, pass);
        return correctPass;
    }

    @Override
    public void run() {
        while (true) {
            if (!readData())
                try {
                    Thread.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

        }
    }

    private boolean readData() {
        try {
            DataInputStream dis = new DataInputStream(client.getInputStream());
            if (dis.available() <= 0) {
                return false;
            }
            short id = dis.readShort();
            OPacket packet = PacketManager.getPacket(id);
            packet.setSocket(client);
            packet.read(dis);
            packet.handle();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    public void serialaizeble() {
        FileOutputStream fis = null;
        try {
            fis = new FileOutputStream("./" + nickname + ".file");
            ObjectOutputStream ois = new ObjectOutputStream(fis);
            ois.writeObject(tasklist);
            //ois.writeObject(TaskWork.getCurrentRemoveTaskList());
            //ois.writeObject(TaskWork.getCurrentContactList());
            ois.writeObject(isp);
            ois.flush();
            ois.close();
            fis.flush();
            fis.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void invalidate() {
        ServerLoader.invalidate(client);
    }


    public void removeTask2(ITask task) {
        ITask temptask = null;
        boolean t = false;
        Long lg = task.getDate().getTime().getTime();
        int i = tasklist.getSize();

        for (int j = 0; j < i; j++) {
            if (tasklist.getTaskToIndex(j).getDate().getTime().getTime() == lg) {
                if (tasklist.getTaskToIndex(j).getName().equals(task.getName()) && tasklist.getTaskToIndex(j).getDescription().equals(task.getDescription())
                        && (tasklist.getTaskToIndex(j).getContactList().getSize() == task.getContactList().getSize())) {

                    int yu = tasklist.getTaskToIndex(j).getContactList().getSize();
                    for (int t1 = 0; t1 < yu; t1++) {
                        if (tasklist.getTaskToIndex(j).getContactList().getContactByIndex(t1).getName().equals(task.getContactList().getContactByIndex(t1).getName()) &&
                                tasklist.getTaskToIndex(j).getContactList().getContactByIndex(t1).getNumber().equals(task.getContactList().getContactByIndex(t1).getNumber())) {
                            t = true;

                        } else {
                            t = false;
                        }
                    }
                    if (t) {
                        temptask = tasklist.getTaskToIndex(j);

                    }

                }
            }
        }

        if (temptask != null) tasklist.removeTask(temptask);

    }

    public int searchIndexTask(ITask task) {
        int yu1=-1;
        boolean t = false;
        Long lg = task.getDate().getTime().getTime();
        int i = tasklist.getSize();

        for (int j = 0; j < i; j++) {
            if (tasklist.getTaskToIndex(j).getDate().getTime().getTime() == lg) {
                if (tasklist.getTaskToIndex(j).getName().equals(task.getName()) && tasklist.getTaskToIndex(j).getDescription().equals(task.getDescription())
                        && (tasklist.getTaskToIndex(j).getContactList().getSize() == task.getContactList().getSize())) {

                    int yu = tasklist.getTaskToIndex(j).getContactList().getSize();
                    for (int t1 = 0; t1 < yu; t1++) {
                        if (tasklist.getTaskToIndex(j).getContactList().getContactByIndex(t1).getName().equals(task.getContactList().getContactByIndex(t1).getName()) &&
                                tasklist.getTaskToIndex(j).getContactList().getContactByIndex(t1).getNumber().equals(task.getContactList().getContactByIndex(t1).getNumber())) {
                            t = true;

                        } else {
                            t = false;
                        }
                    }
                    if (t) {
                        yu1 = j;

                    }

                }
            }
        }

        return yu1;

    }
}
