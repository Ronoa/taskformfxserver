package sample.serverwork;

import sample.taskwork.ICList;
import sample.taskwork.ITList;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class PacketTaskWork  extends OPacket{
    private ITList task;
    private ICList isp;

    public PacketTaskWork(){};
    public PacketTaskWork(ITList task,ICList isp){
        this.task= task;
        this.isp= isp;

    }

    @Override
    public short getId() {
        return 3;
    }

    @Override
    public void write(DataOutputStream dos) throws IOException {
        int countTask=task.getSize();
        int countIsp=isp.getSize();

        //пишем таски
        dos.writeInt(countTask);
        for(int i=0;i<countTask;i++){
            dos.writeUTF(task.getTaskToIndex(i).getName());
            dos.writeUTF(task.getTaskToIndex(i).getDescription());
            dos.writeLong(task.getTaskToIndex(i).getDate().getTime().getTime());

            dos.writeBoolean(task.getTaskToIndex(i).isYvedomlenie());
            dos.writeInt(task.getTaskToIndex(i).getOtlozhit());


            int ty= task.getTaskToIndex(i).getContactList().getSize();
            dos.writeInt(ty);
            for(int j=0;j<ty;j++){
                dos.writeUTF( task.getTaskToIndex(i).getContactList().getContactByIndex(j).getName());
                dos.writeUTF(task.getTaskToIndex(i).getContactList().getContactByIndex(j).getNumber());
            }

        }
        //пишем исполнителей
        dos.writeInt(countIsp);
        for(int i=0;i<countIsp;i++){
            dos.writeUTF(isp.getContactByIndex(i).getName());
            dos.writeUTF(isp.getContactByIndex(i).getNumber());
        }

    }

    @Override
    public void read(DataInputStream dis) throws IOException {

    }

    @Override
    public void handle() {

    }
}
