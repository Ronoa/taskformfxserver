package sample.serverwork;

import sample.taskwork.ICList;
import sample.taskwork.ITList;
import sample.taskwork.TaskList;

import java.io.*;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class PacketAuthorize extends OPacket {
    private String nickname;
    private String pass;
    private boolean flag = false;

    public PacketAuthorize() {

    }

    public PacketAuthorize(String nickname, String pass, Boolean flag) {
        this.nickname = nickname;
        this.pass = pass;
        this.flag = flag;
    }

    @Override
    public short getId() {
        return 1;
    }

    @Override
    public void write(DataOutputStream dos) throws IOException {
        dos.writeBoolean(flag);
    }

    @Override
    public void read(DataInputStream dis) throws IOException {
        nickname = dis.readUTF();
        pass = dis.readUTF();
    }

    @Override
    public void handle() {
        ServerLoader.getHandler(getSocket()).setNickname(nickname);
        flag = ServerLoader.getHandler(getSocket()).isCorrect(nickname, pass);

        ServerLoader.sendPacket(getSocket(), this);
        System.out.println(String.format("[%s] [%s] [%s] ", nickname, pass, flag));

        try {
            Thread.sleep(120);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (flag) {
            FileInputStream fis = null;
            try {
                fis = new FileInputStream("./" + nickname + ".file");
                ObjectInputStream ois = new ObjectInputStream(fis);
                ITList desTask = (ITList) ois.readObject();
                //ITList desRemove= (ITList) ois.readObject();
                //ICList desContact= (ICList) ois.readObject();
                ICList desIsp = (ICList) ois.readObject();
                ois.close();
                fis.close();
                ServerLoader.sendPacket(getSocket(), new PacketTaskWork(desTask, desIsp));
                ServerLoader.getHandler(getSocket()).tasklist = desTask;
                ServerLoader.getHandler(getSocket()).isp = desIsp;

            } catch (FileNotFoundException e) {
                System.out.println("Файл не найден");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

        }


        /////////////////////

        Thread tr = new Thread() {
            public void run() {
                while (true) {


                    Date currentDate = new Date();
                    int num = ServerLoader.getHandler(getSocket()).tasklist.getSize();

                    ITList removeList = new TaskList();

                    ITList yved15List = new TaskList();
                    ITList yved10List = new TaskList();
                    ITList yved5List = new TaskList();

                    ITList tempList = ServerLoader.getHandler(getSocket()).tasklist;

                    //Если список задач не пуст,то
                    if (num > 0) {

                        for (int i = 0; i < num; i++) {
                            Date dateTask = tempList.getTaskToIndex(i).getDate().getTime();
                            if (currentDate.compareTo(dateTask) >= 0)
                                removeList.addTaskEnd(tempList.getTaskToIndex(i));
                        }

                        int numRem = removeList.getSize();
                        //есть че на удаление, если да то
                        if (numRem > 0) {
                            for (int j = 0; j < numRem; j++) {
                                ServerLoader.getHandler(getSocket()).tasklist.removeTask(removeList.getTaskToIndex(j));
                                ServerLoader.sendPacket(getSocket(), new PacketYvedomlebie(removeList.getTaskToIndex(j)));
                            }
                            //сериализуем
                            ServerLoader.getHandler(getSocket()).serialaizeble();
                        }


                         num = ServerLoader.getHandler(getSocket()).tasklist.getSize();

                         tempList = ServerLoader.getHandler(getSocket()).tasklist;

                        for (int i = 0; i < num; i++) {
                            Date dateTask = tempList.getTaskToIndex(i).getDate().getTime();
                            if (
                                    (getDateDiff2(dateTask, currentDate, TimeUnit.MINUTES) == 15)
                                            && (tempList.getTaskToIndex(i).isYvedomlenie() == true)
                                            && (tempList.getTaskToIndex(i).getOtlozhit() == 0)
                            ) {
                                System.out.println("Найдено 15 " + tempList.getTaskToIndex(i).getName() );
                                tempList.getTaskToIndex(i).setYvedomlenie(false);
                                yved15List.addTaskEnd(tempList.getTaskToIndex(i));
                            }
                        }
                        int num15 = yved15List.getSize();

                        for (int i = 0; i < num; i++) {
                            Date dateTask = tempList.getTaskToIndex(i).getDate().getTime();
                            if (
                                    (getDateDiff2(dateTask, currentDate, TimeUnit.MINUTES) == 10)
                                            && (tempList.getTaskToIndex(i).isYvedomlenie() == true)
                                            && (tempList.getTaskToIndex(i).getOtlozhit() == 1)
                            ) {
                                tempList.getTaskToIndex(i).setYvedomlenie(false);
                                yved10List.addTaskEnd(tempList.getTaskToIndex(i));
                            }
                        }
                        int num10 = yved10List.getSize();


                        for (int i = 0; i < num; i++) {
                            Date dateTask = tempList.getTaskToIndex(i).getDate().getTime();
                            if (
                                    (getDateDiff2(dateTask, currentDate, TimeUnit.MINUTES) == 5)
                                            && (tempList.getTaskToIndex(i).isYvedomlenie() == true)
                                            && (tempList.getTaskToIndex(i).getOtlozhit() == 2)
                            ) {
                                tempList.getTaskToIndex(i).setYvedomlenie(false);
                                yved5List.addTaskEnd(tempList.getTaskToIndex(i));
                            }
                        }
                        int num5 = yved5List.getSize();

                        if (num15 > 0)
                            for (int j = 0; j < num15; j++)
                                ServerLoader.sendPacket(getSocket(), new PacketPeresilka(yved15List.getTaskToIndex(j)));

                        if (num10 > 0)
                            for (int j = 0; j < num10; j++)
                                ServerLoader.sendPacket(getSocket(), new PacketPeresilka(yved10List.getTaskToIndex(j)));

                        if (num5 > 0)
                            for (int j = 0; j < num5; j++)
                                ServerLoader.sendPacket(getSocket(), new PacketPeresilka(yved5List.getTaskToIndex(j)));

                        ServerLoader.getHandler(getSocket()).serialaizeble();
                    }


                    //1секунда ожидания
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }

            public long getDateDiff2(Date sobitie, Date currentDate, TimeUnit timeUnit) {

                long diffInMillies = sobitie.getTime() - currentDate.getTime();
                return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
            }
        };
        tr.start();


        /////////////////////


    }
}
